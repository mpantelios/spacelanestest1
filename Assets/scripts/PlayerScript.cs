﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {
	private float stepLength;

	// Use this for initialization
	void Start () {
		stepLength = GameObject.Find ("Game").GetComponent<GameScript>().stepLength;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = this.transform.position;

		if (Input.GetKeyDown (KeyCode.W))
		    {
			transform.position = new Vector3(pos.x, pos.y, pos.z + stepLength);
		}
		if (Input.GetKeyDown (KeyCode.A))
		{
			transform.position = new Vector3(pos.x- stepLength, pos.y, pos.z);
		}      
		if (Input.GetKeyDown (KeyCode.S))
		{      
			transform.position = new Vector3(pos.x, pos.y, pos.z - stepLength);
		}
		
		if (Input.GetKeyDown (KeyCode.D))
		{     
			transform.position = new Vector3(pos.x+ stepLength, pos.y, pos.z);
		}
	}
}
