﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class GameScript : MonoBehaviour {
	public  const float tileSize = 2.0f;
	public float stepLength = 0.0f;
	public GameObject Player;

	private GameObject p;

	// Use this for initialization
	void Start () {
			
		for (int i=0; i<20; i++) {
			GameObject obj = ObjectPooler.current.GetPooledObject();

			if (obj == null) return;

			Renderer rend = obj.transform.GetChild(0).GetComponent<Renderer>();

			obj.transform.position = new Vector3(0,0,i * tileSize * rend.bounds.extents.z);
			obj.SetActive(true);

			stepLength = tileSize * rend.bounds.extents.z;

			if (i==4){
				p = (GameObject)Instantiate(Player);
				p.transform.position = new Vector3(obj.transform.position.x, 1, obj.transform.position.z);

				if (p != null);
					GameObject.Find("Main Camera").GetComponent<FollowTarget>().target = p.transform;
			}
			print ("i="+i);
		}


	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
